import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';
import 'package:passometro/models/patient.dart';
import 'package:passometro/models/user.dart';
import 'package:passometro/pacientes/patient_drawer.dart';
import 'package:passometro/pages/patient_profile/patient_internations_card.dart';
import 'package:passometro/pages/patient_profile/profile_id_card.dart';
import 'package:passometro/widgets/box_list.dart';

enum InternationActions { TROCAR, INTERNAR, ALTA }

class PatientProfile extends StatelessWidget {
  final String patientId;
  final DateFormat _dateFormat = new DateFormat.yMMMMd("pt_BR");

  final User user = User(
      uid: 'adasdasdasdas',
      email: 'paulofavarato@gmail.com',
      name: 'Paulo Busato Favarato',
      gender: 'M');

  PatientProfile({this.patientId});

  @override
  Widget build(BuildContext context) {
    DocumentReference pacRef =
        Firestore.instance.document('pacientes/$patientId');

    return StreamBuilder(
      stream: pacRef.snapshots(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (!snapshot.hasData)
          return Center(child: CircularProgressIndicator());

        Patient patient = Patient.fromSnapshot(snapshot.data);

        return Scaffold(
          appBar: AppBar(
            title: Text("Dados do Paciente"),
            actions: _getActions(context, patient),
          ),
          body: ListView(
            children: <Widget>[
              PatientIdCard(patient: patient), 
              PatientInternationsCard(patient: patient)
            ],
          ),
          drawer: PatientDrawer(user: user, patient: patient),
        );
      },
    );
  }

  List<Widget> _getActions(context, patient) {
    return [
      PopupMenuButton<InternationActions>(
        onSelected: (value) {
          switch (value) {
            case InternationActions.INTERNAR:
              showDialog(
                      context: context,
                      builder: (BuildContext context) =>
                          BoxChooser(BoxShow.EMPTY))
                  .then((box) => _internarPaciente(patient, box))
                  .then((onvalue) => Navigator.of(context).pop());
              break;
            case InternationActions.ALTA:
              break;
            case InternationActions.TROCAR:
              break;
          }
        },
        itemBuilder: (BuildContext context) => _getItems(patient),
      ),
    ];
  }




  List<PopupMenuEntry<InternationActions>> _getItems(Patient patient) {
    if (patient.internado) {
      return [
        const PopupMenuItem(
          value: InternationActions.TROCAR,
          child: Text('Trocar de leito'),
        ),
        const PopupMenuItem(
          value: InternationActions.ALTA,
          child: Text('Alta'),
        ),
      ];
    } else {
      return [
        const PopupMenuItem(
          value: InternationActions.INTERNAR,
          child: Text('Internar'),
        )
      ];
    }
  }

  _internarPaciente(Patient patient, int boxNumber) async {
    DocumentReference pacRef =
        Firestore.instance.document('pacientes/$patientId');
    DocumentReference boxRef =
        Firestore.instance.document('boxes/${boxNumber.toString()}');
    DocumentReference intRef = await pacRef
        .collection('internacoes')
        .add({'when': FieldValue.serverTimestamp(), 'status': 'active'});
    await pacRef.updateData({'internado': true});
    await boxRef.setData({
      'status': 'ocupado',
      'name': patient.name,
      'birth': patient.birth,
      'when': FieldValue.serverTimestamp(),
      'patient_id': patient.id,
      'internation_id': intRef.documentID,
      'gender': patient.gender,
      'number': boxNumber
    });
  }

  _altaPaciente(Patient patient) {}
}
