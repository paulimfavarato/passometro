import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:passometro/models/patient.dart';

class PatientIdCard extends StatelessWidget {
  final Patient patient;
  final DateFormat _dateFormat = new DateFormat.yMMMMd("pt_BR");

  PatientIdCard({Key key, this.patient}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(0))),
      margin: EdgeInsets.symmetric(vertical: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            title: Text(
              "Identificação",
              style: TextStyle(fontSize: 20),
            ),
          ),
          ListTile(
            title: const Text('Nome'),
            subtitle: Text(patient.name),
          ),
          ListTile(
            title: Text('Data de nascimento'),
            subtitle: Text(_dateFormat.format(patient.birth)),
          ),
          ListTile(
            title: const Text('Idade'),
            subtitle: Text(patient.age.toString()),
          ),
        ],
      ),
    );
  }
}
