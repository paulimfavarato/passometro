import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:passometro/models/internation.dart';
import 'package:passometro/models/patient.dart';
import 'package:passometro/widgets/box_list.dart';

class PatientInternationsCard extends StatelessWidget {
  final Patient patient;
  final DateFormat _dateFormat = new DateFormat.yMMMMd("pt_BR");

  PatientInternationsCard({Key key, this.patient}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(0))),
      margin: EdgeInsets.symmetric(vertical: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            title: Text(
              "Internações",
              style: TextStyle(fontSize: 20),
            ),
          ),
          showInternations(patient)
        ],
      ),
    );
  }

  Widget showInternations(Patient patient) {
    return StreamBuilder(
        stream: Firestore.instance
            .collection('pacientes/${patient.id}/internacoes')
            .orderBy("status")
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return ListTile(
              leading: CircularProgressIndicator(),
              title: Text('Carregando internações...'),
            );
          }
          if (!snapshot.hasData || snapshot.data.documents.length == 0)
            return _noInternation(context);

          return ListView.builder(
              itemCount: snapshot.data.documents.length,
              itemBuilder: (context, index) {
                Internation internation =
                    Internation.fromSnapshot(snapshot.data.documents[index]);
                return internation.status == 'active'
                    ? _activeInternation()
                    : _oldInternation();
              });
        });
  }

  Widget _activeInternation() {
    return ListTile(
      title: Text('internação ativa'),
    );
  }

  Widget _oldInternation() {
    return ListTile(
      title: Text('internação passada'),
    );
  }

  Widget _noInternation(context) {
    return ListTile(
      title: Text('Paciente sem internações'),
      trailing: RaisedButton(
        child: Text('Internar'),
        onPressed: () {
          showDialog(
                  context: context,
                  builder: (BuildContext context) => BoxChooser(BoxShow.EMPTY))
              .then((box) => _internarPaciente(patient, box))
              .then((onvalue) => Navigator.of(context).pop());
        },
      ),
    );
  }

  _internarPaciente(Patient patient, int boxNumber) async {
    DocumentReference pacRef =
        Firestore.instance.document('pacientes/${patient.id}');
    return pacRef.collection('internacoes').add({
      'when': FieldValue.serverTimestamp(),
      'status': 'active',
      'box': boxNumber
    });
  }
}
