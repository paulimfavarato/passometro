import 'package:cloud_firestore/cloud_firestore.dart';

class RotinaItem {
  String id;
  String title;
  bool checked;
  DateTime when;
  String details;
  RotinaItem({this.id, this.title, this.checked, this.when, this.details});

  static RotinaItem from(DocumentSnapshot doc) {
    return RotinaItem(
        id: doc.documentID,
        title: doc['title'],
        checked: doc['checked'],
        when: doc['when'],
        details: doc['details']);
  }
}

enum ItemActions { pending, done, delete }

class RotinaAction {
  RotinaItem doc;
  ItemActions action;
  RotinaAction({this.doc, this.action});
}
