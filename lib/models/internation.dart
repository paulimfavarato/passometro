import 'package:cloud_firestore/cloud_firestore.dart';

class Internation {
  final String id;
  final String status;
  final DateTime when;
  final int box;

  Internation({this.id, this.status, this.when, this.box});

  static Internation fromSnapshot(DocumentSnapshot doc) {
    return Internation(
        id: doc.documentID,
        status: doc['status'],
        when: doc['when'],
        box: doc['box']);
  }
}
