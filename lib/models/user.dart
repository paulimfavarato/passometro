import 'package:flutter/material.dart';

class User {
  String uid;
  String name;
  String email;
  String phone;
  String gender;
  String titulo;
  String orgao;
  int number;

  User(
      {@required this.uid,
      @required this.name,
      @required this.email,
      this.phone,
      this.gender,
      this.titulo,
      this.orgao,
      this.number});
}
