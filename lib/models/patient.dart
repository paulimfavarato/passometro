import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Patient {
  final String id;
  final String name;
  final DateTime birth;
  final String gender;
  final bool internado;

  Patient(
      {this.id,
      @required this.name,
      this.birth,
      @required this.gender,
      this.internado});

  int get age {
    Duration diff = DateTime.now().difference(birth);
    return (diff.inDays / 365).floor();
  }

  static Patient fromSnapshot(DocumentSnapshot doc) {
    return Patient(
        id: doc.documentID,
        name: doc['name'],
        birth: doc['birth'],
        gender: doc['gender'],
        internado: doc['internado']);
  }

  static Patient fromBox(DocumentSnapshot doc) {
    return Patient(
        id: doc['patient_id'],
        name: doc['name'],
        birth: doc['birth'],
        gender: doc['gender'],
        internado: doc['internado']);
  }
}
