
import 'package:passometro/models/patient.dart';

class Box {
  int number;
  String status;
  Patient patient;

  Box({this.number, this.status, this.patient = null});
}
