import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:passometro/models/user.dart';
import 'package:passometro/pacientes/internados_list.dart';
import 'package:passometro/pacientes/patient_drawer.dart';
import 'pacientes/pacientes_list.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  void signOut() {
    FirebaseAuth.instance.signOut();
  }

  final User user = User(
      uid: 'adasdasdasdas',
      email: 'paulofavarato@gmail.com',
      name: 'Paulo Busato Favarato',
      gender: 'M');

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Pacientes"),
        bottom: TabBar(
          controller: tabController,
          tabs: <Widget>[Tab(text: "Internados"), Tab(text: "Todos pacientes")],
        ),
      ),
      body: TabBarView(
        controller: tabController,
        children: <Widget>[
          IntenadosList(),
          PacienteList(),
        ],
      ),
      drawer: PatientDrawer(
        user: user,
        patient: null,
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () => Navigator.of(context).pushNamed('/addpatient')),
    );
  }
}
