import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'pages/settings.dart';
import './pacientes/patient_add.dart';
import 'auth/login.dart';
import 'home.dart';
import 'splash.dart';

void main() {
  Intl.defaultLocale = 'pt_BR';
  initializeDateFormatting("pt_BR");
  runApp(MyApp());
}

enum AuthStatus { notSignedIn, signedIn, unknown }

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Widget _handleCurrentScreen() {
    return new StreamBuilder<FirebaseUser>(
        stream: FirebaseAuth.instance.onAuthStateChanged,
        builder: (BuildContext context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return new SplashScreen();
          } else {
            if (snapshot.hasData) {
              return HomePage();
            }
            return new LoginPage();
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Passometro UTI',
      theme: ThemeData(
          primarySwatch: Colors.blue,
          brightness: Brightness.light,
          buttonColor: Colors.blue,
          accentColor: Colors.redAccent),
      home: _handleCurrentScreen(),
      routes: {
        '/splash': (BuildContext context) => SplashScreen(),
        '/login': (BuildContext context) => LoginPage(),
        '/settings': (BuildContext context) => SettingsPage(),
        '/addpatient': (BuildContext context) => PatientAdd()
      },
    );
  }
}
