import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

enum BoxShow { ALL, EMPTY, OCCUPIED }

class BoxChooser extends StatelessWidget {
  final BoxShow show;
  BoxChooser(this.show);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return StreamBuilder(
        stream: _getRef().snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData)
            return Center(child: CircularProgressIndicator());
          if (snapshot.data.documents.length == 0)
            return Text('Adicione alguns items');
          return SimpleDialog(
              title: Text('Escolha um leito'),
              children: snapshot.data.documents
                  .map((box) => DialogDemoItem(
                      icon: Icons.hotel,
                      color: theme.primaryColor,
                      text: "Leito ${box['number']}",
                      onPressed: () {
                        Navigator.of(context).pop<int>(box['number']);
                      }))
                  .toList());
        });
  }

  Query _getRef() {
    switch (show) {
      case BoxShow.EMPTY:
        return Firestore.instance
            .collection('boxes')
            .where('status', isEqualTo: 'livre')
            .orderBy('number');
        break;
      case BoxShow.OCCUPIED:
        return Firestore.instance
            .collection('boxes')
            .where('status', isEqualTo: 'ocupado')
            .orderBy('number');
      default:
        return Firestore.instance.collection('boxes').orderBy('number');
    }
  }
}

class DialogDemoItem extends StatelessWidget {
  const DialogDemoItem(
      {Key key, this.icon, this.color, this.text, this.onPressed})
      : super(key: key);

  final IconData icon;
  final Color color;
  final String text;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return SimpleDialogOption(
      onPressed: onPressed,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(icon, size: 36.0, color: color),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Text(text),
          ),
        ],
      ),
    );
  }
}
