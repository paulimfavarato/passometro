import 'package:flutter/material.dart';
import 'package:passometro/widgets/inputdropdown.dart';
import 'package:intl/intl.dart';

class DateTimePicker extends StatelessWidget {
  const DateTimePicker(
      {this.labelText,
      this.selectedDate,
      this.selectDate,
      this.underline,
      this.selectableDayPredicate});

  final String labelText;
  final DateTime selectedDate;
  final ValueChanged<DateTime> selectDate;
  final SelectableDayPredicate selectableDayPredicate;
  final bool underline;

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        selectableDayPredicate: selectableDayPredicate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate) selectDate(picked);
  }

  @override
  Widget build(BuildContext context) {
    return InputDropdown(
      underline: underline,
      valueText: DateFormat.yMMMd().format(selectedDate),
      onPressed: () {
        _selectDate(context);
      },
    );
  }
}