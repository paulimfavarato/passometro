import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../auth/auth.dart';

class LoginPage extends StatefulWidget {
  LoginPage({this.auth, this.onSignedIn});

  final BaseAuth auth;
  final VoidCallback onSignedIn;
  static const String routeName = "/login";

  @override
  State createState() => _LoginPageState();
}

enum FormType { login, recovery }

class _LoginPageState extends State<LoginPage>
    with SingleTickerProviderStateMixin {
  final formKey = GlobalKey<FormState>();

  String _email;
  String _password;
  FormType _formType = FormType.login;

  AnimationController _iconAnimationController;
  Animation<double> _iconAnimation;

  @override
  void initState() {
    super.initState();
    _iconAnimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    _iconAnimation = CurvedAnimation(
        parent: _iconAnimationController, curve: Curves.bounceOut);
    _iconAnimation.addListener(() => this.setState(() {}));
    _iconAnimationController.forward();
  }

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {
    if (validateAndSave()) {
        if (_formType == FormType.login) {
          FirebaseUser user = await FirebaseAuth.instance.signInWithEmailAndPassword(
            email: _email,
            password: _password
          );
          print('Signed in: ${user}');
        }
    }
  }

  void moveToRecovery() {
    formKey.currentState.reset();
    setState(() {
      _formType = FormType.recovery;
    });
  }

  void moveToLogin() {
    formKey.currentState.reset();
    setState(() {
      _formType = FormType.login;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Passometro UTI'),
      ),
      body: Padding(
        padding: EdgeInsets.all(40.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            FlutterLogo(
              size: _iconAnimation.value * 100,
            ),
            Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: buildInputs() + buildSubmitButtons(context),
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> buildInputs() {
    if (_formType == FormType.login) {
      return [
        TextFormField(
          decoration: InputDecoration(labelText: "E-mail"),
          keyboardType: TextInputType.emailAddress,
          validator: (value) =>
              value.isEmpty ? 'Um email deve ser entrado' : null,
          onSaved: (value) => _email = value,
        ),
        TextFormField(
          decoration: InputDecoration(labelText: "Senha"),
          keyboardType: TextInputType.text,
          obscureText: true,
          validator: (value) =>
              value.isEmpty ? 'Uma senha deve ser entrada' : null,
          onSaved: (value) => _password = value,
        )
      ];
    } else {
      return [
        TextFormField(
          decoration: InputDecoration(labelText: "E-mail"),
          keyboardType: TextInputType.emailAddress,
          validator: (value) =>
              value.isEmpty ? 'Um email deve ser entrado' : null,
          onSaved: (value) => _email = value,
        ),
      ];
    }
  }

  List<Widget> buildSubmitButtons(context) {
    if (_formType == FormType.login) {
      return [
        RaisedButton(
          color: Theme.of(context).primaryColor,
          child: Text('Entrar', style: TextStyle(color: Colors.white),),
          onPressed: validateAndSubmit,
        ),
        GestureDetector(
          child: Text('Esqueci minha senha'),
          onTap: moveToRecovery,
        )
      ];
    } else if (_formType == FormType.recovery) {
      return [
        RaisedButton(
          child: Text("Recuperar senha"),
          onPressed: validateAndSubmit,
        ),
        GestureDetector(
          child: Text('Voltar para login'),
          onTap: moveToLogin,
        )
      ];
    }
  }
}