import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

abstract class BaseAuth {
  Future<String> signInWithEmailAndPassword(String email, String password);
  Future<String> currentUser();
  Future<void> signOut();
  User getUser();
}

class Auth implements BaseAuth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  bool isAuth = false;
  User _user;

  Future<String> signInWithEmailAndPassword(
    String email, String password) async {
    FirebaseUser user = await _firebaseAuth.signInWithEmailAndPassword(
        email: email, password: password);
    return user.uid;
  }

  Future<String> currentUser() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    DocumentSnapshot userData = await Firestore.instance.collection('users').document(user.uid).get();
    _user = new User(
      nome: userData['nome'],
      email: user.email
    );
    return user.uid;
  }

  Future<void> signOut() {
    return _firebaseAuth.signOut();
  }

  getUser() {
    return _user;
  }
}
class User{
  String nome;
  String email;
  User({this.nome, this.email});
}