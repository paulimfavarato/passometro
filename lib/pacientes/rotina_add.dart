import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:passometro/widgets/datetimepicker.dart';
import 'package:intl/intl.dart';

class RotinaAdd extends StatefulWidget {

  final String patientId;

  RotinaAdd({this.patientId});

  @override
  _RotinaAddState createState() => _RotinaAddState();
}

class _RotinaAddState extends State<RotinaAdd> {
  CollectionReference ref;
  final _formKey = GlobalKey<FormState>();
  String _title;
  bool _checked = false;
  String _details;
  FieldValue _when = FieldValue.serverTimestamp();

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit(BuildContext context) async {
    if (validateAndSave()) {
      ref.add({'title': _title, 'checked': _checked, 'when': _when, "details": _details}).then(
          (onValue) {
        Navigator.of(context).pop();
      });
    }
  }

@override
  void initState() {
    super.initState();
    ref =  Firestore.instance.collection('pacientes').document(widget.patientId).collection('rotina');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Novo item de Rotina'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.check),
            onPressed: () => validateAndSubmit(context),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(
                    hintText: 'Descrição',
                    labelText: 'Digite o item da rotina'),
                keyboardType: TextInputType.text,
                validator: (value) =>
                    value.isEmpty ? 'Uma descrição deve ser entrada' : null,
                onSaved: (value) => _title = value,
              ),
              SizedBox(height: 8.0),
              TextFormField(
                  decoration: InputDecoration(labelText: 'Data'),
                  enabled: false,
                  initialValue: DateFormat.yMd().format(DateTime.now())),
              SizedBox(height: 8.0),
              TextFormField(
                onSaved: (value) => _details = value,
                decoration: InputDecoration(labelText: 'Detalhes'),
                keyboardType: TextInputType.multiline,
                maxLines: 4,
              ),
              SizedBox(height: 8.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Checkbox(
                    value: _checked,
                    tristate: false,
                    onChanged: (bool value) {
                      setState(() {
                        _checked = value;
                      });
                    },
                  ),
                  Text("Já realizado")
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
