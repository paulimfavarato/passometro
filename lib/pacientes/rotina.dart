import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:passometro/models/rotina_item.dart';
import 'package:passometro/pacientes/rotina_add.dart';
import 'package:passometro/widgets/datetimepicker.dart';
import 'package:intl/intl.dart';

class RotinaPage extends StatefulWidget {
  final String patientId;

  RotinaPage({this.patientId});

  @override
  _RotinaPageState createState() => _RotinaPageState();
}

class _RotinaPageState extends State<RotinaPage> {
  DateTime _fromDate = DateTime.now();
  CollectionReference _ref;

  @override
  void initState() {
    _ref = Firestore.instance
        .collection('pacientes')
        .document(widget.patientId)
        .collection('rotina');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: DateTimePicker(
          selectedDate: _fromDate,
          underline: false,
          selectableDayPredicate: (DateTime val) =>
              val.isAfter(DateTime.now()) ? false : true,
          selectDate: (DateTime date) {
            setState(() {
              _fromDate = date;
            });
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) =>
                RotinaAdd(patientId: widget.patientId),
            fullscreenDialog: true,
          ));
        },
      ),
      body: StreamBuilder<QuerySnapshot>(
          stream: _ref.orderBy('when', descending: true).snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (!snapshot.hasData)
              return Center(child: CircularProgressIndicator());
            if (snapshot.data.documents.length == 0)
              return _notItemMsg('Adicione alguns items');
              print("build");
            List<Widget> itemWidgets = [];
            var documents = snapshot.data.documents;

            DateTime lastDay = _fromDate;
            for (int i = 0; i < documents.length; i++) {
              if (i == 0 && _fromDate.day == DateTime.now().day)
                itemWidgets.add(ListTile(title: Text('Ultimos')));
              RotinaItem item = RotinaItem.from(documents[i]);
              if (item.when.day == _fromDate.day) {
                itemWidgets.add(_getListItem(item));
              }
              if (item.when.day < lastDay.day) {
                itemWidgets.add(_getSection(item.when));
                itemWidgets.add(_getListItem(item));
              }
              lastDay = item.when;
            }

            if (itemWidgets.length == 0)
              return _notItemMsg('Sem itens de rotina neste dia ...');
            return ListView.builder(
                itemCount: itemWidgets.length,
                itemBuilder: (BuildContext context, index) {
                  return itemWidgets[index];
                });
          }),
    );
  }

  Widget _getSection(DateTime when) {
    return ListTile(
      title: Text(
        DateFormat.yMd().format(when),
        style: TextStyle(fontSize: 14.0),
      ),
    );
  }

  Widget _notItemMsg(String msg) {
    return Center(child: Text(msg, style: TextStyle(fontSize: 17.0)));
  }

  Widget _getListItem(RotinaItem item) {
    return ListTile(
      key: Key(item.id),
      title: Text(item.title),
      subtitle: (item.details == null || item.details == '')
          ? null
          : Text(item.details),
      leading:
          Icon(item.checked ? Icons.check_box : Icons.check_box_outline_blank),
      trailing: PopupMenuButton<RotinaAction>(
          padding: EdgeInsets.zero,
          onSelected: showMenuSelection,
          itemBuilder: (BuildContext context) => <PopupMenuItem<RotinaAction>>[
                PopupMenuItem<RotinaAction>(
                  value: RotinaAction(doc: item, action: ItemActions.done),
                  child: const Text('Marcar realizado'),
                ),
                PopupMenuItem<RotinaAction>(
                  value: RotinaAction(doc: item, action: ItemActions.pending),
                  child: const Text('Marcar não realizado'),
                ),
                PopupMenuItem<RotinaAction>(
                  value: RotinaAction(doc: item, action: ItemActions.delete),
                  child: const Text('Apagar Item'),
                ),
              ]),
      onTap: () {},
    );
  }

  void showMenuSelection(RotinaAction item) {
    switch (item.action) {
      case ItemActions.done:
        _ref.document(item.doc.id).updateData({'checked': true});
        // .then((onValue) => showInSnackBar('O item foi marcado como realizado'));
        break;
      case ItemActions.pending:
        _ref.document(item.doc.id).updateData({'checked': false});
        // .then(
        // (onValue) => showInSnackBar('O item foi marcado como pendente'));
        break;
      case ItemActions.delete:
        _ref.document(item.doc.id).delete();
        // .then((onValue) => showInSnackBar('O item foi deletado'));
        break;
    }
  }

  void showInSnackBar(String value) {
    // _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(value)));
  }
}
