import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:passometro/models/patient.dart';
import '../pacientes/paciente_list_item.dart';

class PacienteList extends StatelessWidget {
  Widget _buildListItem(BuildContext context, DocumentSnapshot document) {
    Patient paciente = Patient(
        id: document.documentID,
        name: document['name'],
        birth: document['birth'],
        gender: document['gender']);
    return PacienteListItem(paciente);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: Firestore.instance.collection('pacientes').snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Center(child: CircularProgressIndicator());
          return ListView.builder(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            itemExtent: 80.0,
            itemCount: snapshot.data.documents.length,
            itemBuilder: (context, index) =>
                _buildListItem(context, snapshot.data.documents[index]),
          );
        });
  }
}
