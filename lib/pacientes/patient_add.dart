import 'package:flutter/material.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class PatientAdd extends StatefulWidget {
  @override
  _PatientAddState createState() => _PatientAddState();
}

class _PatientAddState extends State<PatientAdd> {
  final TextEditingController _controller = new TextEditingController();
  final _formKey = GlobalKey<FormState>();

  String _gender = 'M';
  String _name;
  DateTime _birth;

  List<DropdownMenuItem<String>> items = [
    DropdownMenuItem(
      child: Text('Masculino'),
      value: 'M',
    ),
    DropdownMenuItem(
      child: Text('Feminino'),
      value: 'F',
    )
  ];

  @override
  void initState() {
    super.initState();
  }

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit(BuildContext context) async {
    if (validateAndSave()) {
      Firestore.instance
          .collection('pacientes')
          .add({'name': _name, 'birth': _birth, 'gender': _gender, 'internado': false})
          .then((onValue) {
            Navigator.of(context).pop();
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Novo paciente'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () => validateAndSubmit(context),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Text(
                'Identificação do paciente',
                style: TextStyle(fontSize: 20.0),
              ),
              SizedBox(height: 16.0),
              TextFormField(
                decoration: InputDecoration(
                    hintText: 'Nome do Paciente', labelText: 'Nome'),
                keyboardType: TextInputType.text,
                validator: (value) =>
                    value.isEmpty ? 'Um nome deve ser entrado' : null,
                onSaved: (value) => _name = value,
              ),
              SizedBox(height: 8.0),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  new Expanded(
                      child: TextFormField(
                    decoration: new InputDecoration(
                        hintText: 'Entre com a data de Nascimento',
                        labelText: 'Nascimento'),
                    controller: _controller,
                    keyboardType: TextInputType.datetime,
                    validator: (value) =>
                        value.isEmpty ? 'Uma data deve ser entrada' : null,
                    onSaved: (value) => _birth = convertToDate(value),
                  )),
                  new IconButton(
                    icon: new Icon(Icons.calendar_today),
                    tooltip: 'Choose date',
                    onPressed: (() {
                      _chooseDate(context, _controller.text);
                    }),
                  )
                ],
              ),
              SizedBox(height: 8.0),
              DropdownButtonFormField(
                decoration: InputDecoration(labelText: 'Sexo'),
                items: items,
                value: _gender,
                hint: Text('Escolha uma opção'),
                onSaved: (value) => _gender = value,
                onChanged: (value) {
                  setState(() {
                    _gender = value;
                  });
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  Future _chooseDate(BuildContext context, String initialDateString) async {
    var now = new DateTime.now();
    var initialDate = convertToDate(initialDateString) ?? now;
    initialDate = (initialDate.year >= 1900 && initialDate.isBefore(now)
        ? initialDate
        : now);

    var result = await showDatePicker(
        context: context,
        initialDate: initialDate,
        firstDate: new DateTime(1900),
        lastDate: new DateTime.now());

    if (result == null) return;

    setState(() {
      _controller.text = new DateFormat.yMd().format(result);
    });
  }

  DateTime convertToDate(String input) {
    try {
      var d = new DateFormat.yMd().parseStrict(input);
      return d;
    } catch (e) {
      return null;
    }
  }
}
