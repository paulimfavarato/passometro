import 'package:flutter/material.dart';
import 'package:passometro/models/patient.dart';
import 'package:passometro/models/user.dart';
import 'package:passometro/pacientes/rotina.dart';

class PatientDrawer extends StatelessWidget {
  final Patient patient;
  final User user;

  PatientDrawer({this.patient, this.user});

  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: new ListView(
        children: [_getHeader()] +
            _getMainSection(context) +
            _getPatientSection(context) +
            _getBottomSection(context),
      ),
    );
  }

  Widget _getHeader() {
    return UserAccountsDrawerHeader(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage('assets/drawerheaderbg.jpg'),
        ),
      ),
      accountName: new Text(
        user.name,
        style: TextStyle(fontSize: 18.0),
      ),
      accountEmail: Text(user.email),
      currentAccountPicture: CircleAvatar(
        backgroundColor: Colors.white,
        // child: Text(
        //   user.name[0],
        //   style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
        // ),
        child: Image.asset(
          'assets/medical.png',
          height: 52.0,
        ),
        radius: 10.0,
      ),
    );
  }

  List<Widget> _getMainSection(context) {
    return [
      ListTile(
          leading: new Icon(Icons.home),
          title: new Text("Pacientes"),
          onTap: () {
            Navigator.popUntil(context, ModalRoute.withName('/'));
          }),
      ListTile(
          leading: new Icon(Icons.star),
          title: new Text("Favoritos"),
          onTap: () {
            Navigator.popUntil(context, ModalRoute.withName('/'));
          })
    ];
  }

  List<Widget> _getPatientSection(context) {
    if (patient == null) return [];
    return [
      Divider(),
      ListTile(
        title: Text(patient.name,
            style: TextStyle(
              color: Colors.black38,
            )),
      ),
      ListTile(
          leading: Icon(Icons.timeline),
          title: Text("Linha do tempo"),
          onTap: () {}),
      ListTile(
        leading: Icon(Icons.check_circle_outline),
        title: Text("Rotina Médica"),
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
                builder: (BuildContext context) =>
                    RotinaPage(patientId: patient.id)),
          );
        },
      )
    ];
  }

  List<Widget> _getBottomSection(context) {
    return [
      Divider(),
      ListTile(
          leading: Icon(Icons.settings),
          title: Text("Configuracões"),
          onTap: () {}),
      ListTile(
          leading: Icon(Icons.exit_to_app), title: Text("Sair"), onTap: () {}),
    ];
  }
}
