import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:passometro/models/box.dart';
import 'package:passometro/models/patient.dart';
import 'package:passometro/pacientes/internados_list_item.dart';

class IntenadosList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: Firestore.instance
            .collection('boxes')
            .orderBy('number')
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return const Text('Carregando...');
          return ListView.builder(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            itemExtent: 80.0,
            itemCount: snapshot.data.documents.length,
            itemBuilder: (context, index) =>
                _buildListItem(context, snapshot.data.documents[index]),
          );
        });
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot document) {
    Box box = Box(
        number: int.parse(document.documentID),
        status: document['status'],
        patient:
            document['status'] == 'ocupado' ? Patient.fromBox(document) : null);
    if (box.status == 'ocupado') {
      return InternadoListItem(box);
    }
    return _freeBox(box);
  }

  Widget _freeBox(Box box) {
    return ListTile(
      leading: CircleAvatar(child: new Text(box.number.toString())),
      title: new Text(
        "Leito Livre",
        style: TextStyle(color: Colors.grey),
      ),
    );
  }
}
