import 'package:flutter/material.dart';
import 'package:passometro/models/patient.dart';
import 'package:passometro/pages/patient_profile/patient_profile.dart';

class PacienteListItem extends StatelessWidget {
  final Patient _patient;

  PacienteListItem(this._patient);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: ListTile(
          leading: CircleAvatar(child: new Text(_patient.name[0])),
          title: new Text(_patient.name),
          subtitle: Text("${_patient.age} anos")),
      onTap: () => Navigator.of(context).push(
            MaterialPageRoute(
                builder: (context) => PatientProfile(patientId: _patient.id)),
          ),
    );
  }
}
