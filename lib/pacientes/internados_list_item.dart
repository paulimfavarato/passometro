import 'package:flutter/material.dart';
import 'package:passometro/models/box.dart';
import 'package:passometro/pages/patient_profile/patient_profile.dart';

class InternadoListItem extends StatelessWidget {
  final Box _box;

  InternadoListItem(this._box);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: ListTile(
            leading: CircleAvatar(child: new Text(_box.number.toString())),
            title: new Text(_box.patient.name),
            subtitle: Text(_box.patient.age.toString() + ' anos')),
        onTap: () => Navigator.of(context).push(
            MaterialPageRoute(
                builder: (context) => PatientProfile(patientId: _box.patient.id)),
          ),);
  }
}
